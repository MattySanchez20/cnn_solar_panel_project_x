# CNN Binary Classification of Solar Panels

## Abstract:

A Convolutional Neural Network (CNN) was made to classify images of solar panels as clean or dirty (0 or 1). The images to train, test and validate the model were obtained from Kaggle. A CNN was used over a Multi-Layer Perceptron (MLP) due to high variance in solar panel position within the image (the model required translational invariance). The number of epochs was 20; the regularisation parameter (L2) was 0.6. The model contained 1 convolutional layer using a Relu activation function and 1 max pooling layer. Predictions were made using a binary-cross-entropy function with a sigmoid activation function in a dense layer. The optimisation function was Adam’s Algorithm. The accuracy, precision and recall were 0.75, 0.67 and 0.84 respectively.

## Introduction:

### Neural Network Architecture:

A neural network is comprised of the following:
-	Nodes
-	Neurons
-	Input layer
-	Hidden layer(s)
-	Output layer

The role of the input layer is to receive raw data from a data source or pipeline. This layer does not perform any computation on the data, rather, it feeds the data to the hidden layer(s). Hidden layers are a set of nodes at are not exposed to raw data input. The layers perform computations on the data to ultimately feed results to the output layer. The output layer is the final layer of the model that provides the result of the model.
The layers described above are composed of neurons and nodes. A node is a computational unit that has weighted input connections from another node and outputs data. These connections are called neurons and are characterised by weight, bias and an activation function. The weight of the neuron is a numerical magnitude that describes the effect of a neuron on a subsequent neuron. The bias of a neuron offsets the result to force an output from the activation function.

### Activation Functions:

An activation function determines whether a neuron is activated or not. This means that insignificant input strengths are not passed to the neuron. It transforms an input signal from a node by summing the weighted input and feeding it to a next hidden layer, or as an output.
There are various types of activation functions, however for binary classification a sigmoid function is commonly used. A sigmoid function can be seen in figure 1. The distribution resembles an S shape with an output between 0 and 1. This makes a sigmoid function suitable for outcomes that are probabilistic since all probabilities lay between 0 and 1. To force a binary output, a threshold value is chosen so that values above the threshold are classified differently to values below the threshold.

![Sigmoid](sigmoid_function.jpg)
_Figure 1_

Another common activation function is a Rectified Linear Unit (ReLU) function. A ReLU function can be seen in figure 2. Due to the distribution of the ReLU function, x values below 0 do not activate the neuron. The disadvantage of the ReLU function is that neurons can die in the process of backwards propagation (updating neuron weights and biases using gradient of the activation function) as the gradient of values below 0 is 0.

![relu](relu_function.jpg)
_Figure 2_

### MLP vs CNN:

MLPs and CNNs are both neural networks with the architecture described above. A major difference is that MLPs take vectors as input and CNNs take tensors as input: this allows CNNs to understand relations between nearby pixels of an image whereas MLPs cannot i.e., MLPs disregard spatial information due to the flattening of vectors. Since the position of the solar panels within the frame can vary, a CNN was chosen for the algorithm.

### Optimisation Algorithm:

An optimisation algorithm is used to reduce the time for the model to obtain results. It makes computation time shorter by change the attributes of a neural network such as weights and learning rate to reduce the loss of the model (loss is the measure of the error of the model).
The optimisation algorithm used for the model was the Adam algorithm since it required faster computation time and fewer parameters for tuning. Figure 3 shows a comparison between most common optimisation algorithms (Adam’s algorithm has the lowest training cost).

![adams](opfunc_CNN.png)

_Figure 3_

## Objectives:

The 2 main objectives of this project were to:
-	Classify images of dirty or clean solar panels
-	Optimise the model using regularisation and by altering model complexity

## Method

The images of solar panels were obtained from Kaggle. Any images with incompatible file extensions were removed. The data was then split into batches to handle workflow. The algorithm recognised 2 classes: dirty and clean (1 and 0 respectively). A batch containing the image and label can be seen in figure 4.

![solarpanels](solarimagesCNN.png)

_Figure 4_

Since the images have not been pre-processed, the images were normalised so that the pixel intensity values ranged between 0 and 1. This makes the process of backward propagation during training faster. After being normalised, the data was split into training, testing and validation sets with a ratio of 0.8, 0.1, 0.1 respectively. The split ensures that the model has sufficient data to be trained on while having enough to validate and test the performance of the algorithm.

### Building the Model:

The baseline CNN contained 1 convolutional layer using a ReLU activation function (no form of regularisation was used for the baseline model). ReLU was used to help prevent the exponential growth in computation required for the algorithm to run. A max pooling layer was added in order to avoid the model overfitting. The output layer contained a sigmoid activation function due to the binary nature of the classification. The number of epochs was set to 20 since any further increase resulted in a rise in the validation loss. The structure can be seen in figure 5. The baseline model was improved by adding a L2 regularisation of 0.6. The number of epochs was kept the same (20).  Both models used Binary-Loss-Entropy function because the output is binary. Furthermore, the model used Adam’s optimisation algorithm.

![layers](parametersforCNN.png)

_Figure 5_

## Results and Analysis:

![loss_epoch](loss_vs_epoch.jpg)
_Figure 6_

As seen in figure 6, the loss of the validation set was substantially higher than the loss in the training set and it did not converge regardless of increasing epoch (minimum val loss = 0.46, minimum training loss = 0.16). This is evidence of overfitting and is caused by the model’s poor ability to classify unseen images. It is also caused by the model memorising the current training data.

![accuracy_vs_epoch](accuracy_vs_epoch.jpg)
_Figure 7_

Further evidence of overfitting can be seen in figure 7 where the training accuracy, past epoch 17, differs by ~0.18 from the validation set (max training accuracy = 0.98, max validation accuracy = 0.8). This is caused by the model being very accurate when classifying the training set, but inaccurate when classifying unseen images within the validation set.

The accuracy, precision and recall obtained when testing the model against the testing set was 0.75, 0.67 and 0.84 respectively.

## Future Work

Due to the overfitting of the model (evidence by high validation loss), the algorithm can be further penalised for having a large variance. This can be done by increasing the regularisation parameters. It can also be achieved by increasing the number of images being used to train the model, either by implementing data augmentation or by scraping more images of clean and dirty solar panels. Data augmentation involves changing image properties (such as brightness or rotation) to increase the variety of images being used by the model for training. Another penalisation technique is to add a dropout layer to the CNN. This layer temporarily prevents neuron connections to random nodes to nullify the effect of some parameters on the model: this prevents the first batch of training images from affecting the models disproportionately, thus preventing overfitting.

## Conclusion

A CNN model was developed to classify an image of a solar panel as clean or dirty. The model was comprised of one convolutional layer using an L2 regularisation and a ReLU activation function. Subsequently, the model had a max pooling layer and a dense layer with a sigmoid activation function. The accuracy, precision and recall were 0.75, 0.67, 0.84 respectively. The model was overfitting, evidenced by a high minimum validation loss of 0.46. Overfitting can be diminished by using data augmentation, adding a dropout layer and scraping more solar panel images.

## Reflection

During this project, I have learned/improved the following skills:
-	CNNs and MLPs
-	Tensorflow
-	Batch pipelines
-	Stakeholder communication
-	Time management
-	How to use analogies to simplify hard concepts

I am very pleased with this project since I have managed to learn all the above in the space of a week. I learned from my previous mistake that I committed in the previous machine learning lab where I spent too much time trying to improve the model instead if prioritising other tasks with closer deadlines. My method for this project was to code a working algorithm and to postpone the optimisation process until the other tasks were completed: this allowed for progress in multiple tasks and avoided neglect.
If I had to do this project again, I would spend 1 day less choosing what project I was going to do. I would also prioritise learning the theory of CNNs and MLPs before starting to build the model in python since it would have given me a better understanding of the code. 
